ARG DEBIAN_VERSION
ARG RUST_VERSION
ARG TEXLIVE_IMAGE

FROM rust:${RUST_VERSION}-slim-${DEBIAN_VERSION} AS builder

ARG TEXLAB_VERSION
ARG TECTONIC_VERSION

WORKDIR /usr/src/app
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        libfontconfig1-dev \
        libgraphite2-dev \
        libharfbuzz-dev \
        libicu-dev \
        libssl-dev\
        tar \
        zlib1g-dev && \
    curl -fLo texlab.tar.gz \
        https://github.com/latex-lsp/texlab/archive/refs/tags/v${TEXLAB_VERSION}.tar.gz && \
    curl -fLo tectonic.tar.gz \
        https://github.com/tectonic-typesetting/tectonic/archive/refs/tags/tectonic@${TECTONIC_VERSION}.tar.gz && \
    tar xzf texlab.tar.gz && \
    tar xzf tectonic.tar.gz && \
    mv texlab-${TEXLAB_VERSION} texlab && \
    mv tectonic-tectonic-${TECTONIC_VERSION} tectonic && \
    cd texlab && cargo build --release && \
    cd ../tectonic && cargo build --release --features external-harfbuzz


FROM ${TEXLIVE_IMAGE}

COPY --from=builder /usr/src/app/texlab/target/release/texlab /usr/src/app/tectonic/target/release/tectonic /usr/local/bin/
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        libfontconfig1 \
        libgraphite2-3 \
        libharfbuzz-icu0 \
        libpng16-16 \
        libssl3 \
        ncat \
        zlib1g

CMD ["texlab"]
